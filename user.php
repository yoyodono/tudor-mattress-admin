<?php require "./include/koneksi.php"; ?>
		  <div class="col-md-10">

	  			<div class="row">

	  				<div class="col-md-6">
	  					<div class="content-box-large">
			  				<div class="panel-heading">
								<div class="panel-title">User Lists</div>
								
								<div class="panel-options">
									<a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
									<a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
								</div>
							</div>
			  				<div class="panel-body">
			  					<table class="table table-striped">
					              <thead>
					                <tr>
					                  <th>Username</th>
					                  <th>Last Login</th>
					                  <th>Posisi</th>
					                  <th></th>
					                </tr>
					              </thead>
					              <tbody>
					                <tr>
					                	<?php
											$sqluser = 'SELECT * FROM user';		
											$resultuser= mysql_query($sqluser);	
											while($user = mysql_fetch_array($resultuser)){
												echo "<td>".$user['username']."</td>
												<td>".$user['lastlogin']."</td>
												<td>".$user['jabat']."</td>
												<td>
													<a href='proses/user.php?action=disable&username=".$user['username']."' title='Disable'><i class='glyphicon glyphicon-ban-circle'></i></a></td></tr>";
											}
					                	?>
					                </tr>
					              </tbody>
					            </table>
			  				</div>
			  			</div>
	  				</div>

	  				<div class="col-md-5">
	  					<div class="content-box-large">
			  				<div class="panel-heading">
					            <div class="panel-title">Add New User</div>
					          
					            <div class="panel-options">
					              <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
					              <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
					            </div>
					        </div>
			  				<div class="panel-body">
			  					<form class="form-horizontal" role="form" action="proses/user.php?action=add" method="POST">

				                <?php if($_GET['notif'] && $_GET['notif'] == 1){ ?>
									<div class="alert alert-success">
									  <strong>User </strong> berhasil disimpan
									</div>
				                <?php } else if($_GET['notif'] && $_GET['notif'] == 2){ ?>
									<div class="alert alert-danger">
									  <strong>Username </strong> telah terdaftar
									</div>
				                <?php } else if($_GET['notif'] && $_GET['notif'] == 0){ ?>
									<div class="alert alert-danger">
									  <strong>User </strong> gagal tersimpan !!
									</div>
				                <?php } else if($_GET['notif'] && $_GET['notif'] == 0){ ?>
									<div class="alert alert-danger">
									  <strong>User </strong> gagal tersimpan password dan password confirmation harus sama !!
									</div>
								<?php } ?>

								  <div class="form-group">
								    <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
								    <div class="col-sm-10">
								      <input type="input" required class="form-control" id="username" name="username" placeholder="Username">
								    </div>
								  </div>
								  <div class="form-group">
								    <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
								    <div class="col-sm-10">
								      <input type="password" required class="form-control" id="password" name="pass" placeholder="Password">
								    </div>
								  </div>
								  <div class="form-group">
								    <label for="inputPassword3" class="col-sm-2 control-label">Konfirmasi Password</label>
								    <div class="col-sm-10">
								      <input type="password" required class="form-control" id="password" name="pass_confirm" placeholder="Konfirmasi Password">
								    </div>
								  </div>

								  <div class="form-group">
								    <div class="col-sm-offset-2 col-sm-10">
								      <button type="submit" class="btn btn-primary">Simpan</button>
								    </div>
								  </div>
								</form>
			  				</div>
			  			</div>
	  				</div>



		  		</div>
		  </div>


	  		<!--  Page content -->

