-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Inang: localhost
-- Waktu pembuatan: 02 Okt 2016 pada 16.50
-- Versi Server: 5.5.52-0ubuntu0.14.04.1
-- Versi PHP: 5.5.9-1ubuntu4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `tudor`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `image`
--

CREATE TABLE IF NOT EXISTS `image` (
  `id` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `origin` varchar(255) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `image`
--

INSERT INTO `image` (`id`, `product_id`, `origin`, `thumb`, `created_at`, `updated_at`) VALUES
('5trftf0ppoamgn76b6di', 3, '5trftf0ppoamgn76b6di.jpg', '', '2016-09-24 12:13:44', '0000-00-00 00:00:00'),
('r79emgwtu6xvdipoeo8g', 4, 'r79emgwtu6xvdipoeo8g.jpg', '', '2016-09-24 12:15:59', '0000-00-00 00:00:00'),
('w9dfdf2w7ar5x7q7la2c', 3, 'w9dfdf2w7ar5x7q7la2c.jpg', '', '2016-09-24 12:13:54', '0000-00-00 00:00:00'),
('wl2isrl9b1o15p9wzwul', 4, 'wl2isrl9b1o15p9wzwul.png', '', '2016-09-24 12:15:51', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `detail` varchar(300) NOT NULL,
  `price` int(11) NOT NULL,
  `default_img` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type_id` (`type_id`),
  KEY `default_img` (`default_img`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `product`
--

INSERT INTO `product` (`id`, `type_id`, `name`, `detail`, `price`, `default_img`) VALUES
(3, 4, 'Kasur Jumbo', 'Kasur ini merupakan kasur dengan kualitas terbaik, ukuran besar dan harga terjangkau', 1200000, '5trftf0ppoamgn76b6di'),
(4, 4, 'Kasur Gajah', 'Kasur Gajah adalah solusi untuk Anda yang bertubuh seperti Gajah, dilengkapi dengan air bag sistem memungkinkan Anda tetap aman meskipun terjatuh dari kasur.', 3000000, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_type`
--

CREATE TABLE IF NOT EXISTS `product_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `size` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `product_type`
--

INSERT INTO `product_type` (`id`, `name`, `size`) VALUES
(1, 'Single', '100 x 200'),
(2, 'Super Single / Twin', '120 x 200'),
(3, 'Queen', '160 x 200'),
(4, 'King', '180 x 200'),
(5, 'Estra King', '200 x 200');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `lastlogin` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `jabat` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `pass`, `lastlogin`, `jabat`) VALUES
(1, 'alkunjo', 'á"áZƒ&®·‘\rm(g”', '2016-10-02 08:25:30', 'admin');

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `product_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
