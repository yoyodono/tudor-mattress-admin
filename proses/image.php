<?php
	require "../include/koneksi.php";
	$id = '';
	
	//random ID generator
	function random_string($length) {
	  $key = '';
	  $keys = array_merge(range(0, 9), range('a', 'z'));

	  for ($i = 0; $i < $length; $i++) {
	  	$key .= $keys[array_rand($keys)];
	  }
	  return $key;
	}

	function default_image($image_id, $product_id){
		$q = 'SELECT count(*) jml FROM image WHERE id="'.$image_id.'"';
		$h = mysql_query($q);
		$r = mysql_fetch_array($h, MYSQL_ASSOC);
		
		//check whether image exist
		if ($r) {
			$qpd = 'UPDATE product
				SET default_img = "'.$image_id.'"
				WHERE product_id = "'.$product_id.'"';
			$hpd = mysql_query($qpd);
			$rpd = mysql_fetch_assoc($hpd);
			header("location: ../index.php?p=image&prod_id=".$product_id."&ntf=6");
		}else{
			$qpd = 'UPDATE product
				SET default_img = "(SELECT MAX(id) FROM image WHERE product_id="'.$product_id.'")"
				WHERE product_id = "'.$product_id.'"';
			$hpd = mysql_query($qpd);
			$rpd = mysql_fetch_assoc($hpd);
		}
	}

	//header to image page
	if($_GET['action'] == "index"){
		$prod_id = $_GET['prod_id'];
		header("location: ../index.php?p=image&prod_id=".$_GET['prod_id']);
	}

	//for add new image
	elseif ($_GET['action'] == "add") {
		
		$prod_id = $_GET['i'];
		$id = random_string(20);

		$target_file = $path . basename($_FILES["imgUpl"]["name"]);
		
		$temp = explode(".", $_FILES['imgUpl']['name']);
		$name = $id.'.'.end($temp);
		$newTargetFile = $path . $name;

		$uploadOk = 1;
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
		
		// Check if image file is fake image
		if(isset($_POST["submit"])) {
		  $check = getimagesize($_FILES["imgUpl"]["tmp_name"]);
		  if($check == false) {
		    $uploadOk = 0;
		  	header("location: ../index.php?p=image&prod_id=".$prod_id."&ntf=1");
		  }
		}

		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {
		  $uploadOk = 0;
		  header("location: ../index.php?p=image&prod_id=".$prod_id."&ntf=2");
		}

		//upload image to dir
		if ($uploadOk == 0) {
		  header("location: ../index.php?p=image&prod_id=".$prod_id."&ntf=3");
		  // echo "Sorry, your file was not uploaded.";
		}else {
		  if (move_uploaded_file($_FILES["imgUpl"]["tmp_name"], '.'.$newTargetFile)) {
		  	$s = 'INSERT INTO image 
		  				SET
		  				id = "'.$id.'",
		  				product_id = "'.$prod_id.'",
		  				origin = "'.$name.'",
		  				created_at = (select now())';
		  	$r = mysql_query($s);
		  	if ($r) {
		  		header("location: ../index.php?p=image&prod_id=".$prod_id."&ntf=4");
		  	}else{
		  		header("location: ../index.php?p=image&prod_id=".$prod_id."&ntf=3");
		  	}
		  } else {
		  	// echo "Sorry, there was an error uploading your file.";
		  	header("location: ../index.php?p=image&prod_id=".$prod_id."&ntf=3");
			}
		}
	}

	//delete image
	elseif ($_GET['action'] == 'del') {
		$id = $_GET['imgid'];
		$prod_id = $_GET['prod_id'];
		//get file name
		$q = 'SELECT origin, product_id from image where id="'.$id.'" limit 1';
		$h = mysql_query($q);
		$r = mysql_fetch_assoc($h);
		// echo $r['origin'];
		
		//delete the file
		unlink('.'.$path.$r['origin']);
		

		if (!file_exists($path.$r['origin'])) {
			//delete image from db
			$qd = 'DELETE FROM image WHERE id="'.$id.'"';
			$hd = mysql_query($qd);
			$rd = mysql_fetch_assoc($hd);
		  header("location: ../index.php?p=image&prod_id=".$prod_id."&ntf=5");
		}
	}

	//set default image
	elseif ($_GET['action'] == 'dflt') {
		$qpd = 'UPDATE product
		SET default_img = "'.$_GET['imgid'].'"
		WHERE id = "'.$_GET['prod_id'].'"';
		$hpd = mysql_query($qpd);
		if($hpd) {
			header("location: ../index.php?p=image&prod_id=".$_GET['prod_id']."&ntf=6");
		}else{

		}
	}
?>