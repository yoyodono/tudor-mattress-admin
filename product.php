<?php 
	require "./include/koneksi.php"; 
	// session_start();
	//echo $_SESSION['type'];
?>
		  <div class="col-md-10">

	  			<div class="row">

	  				<div class="col-md-6">
	  					<div class="content-box-large">
			  				<div class="panel-heading">
								<div class="panel-title">Product Lists</div>
								
								<div class="panel-options">
									<a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
									<a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
								</div>
							</div>
			  				<div class="panel-body">
			  					<table class="table table-striped">
					              <thead>
					                <tr>
					                  <th>Name</th>
					                  <th>Price</th>
					                  <th>Tipe</th>
					                  <th colspan="2">Aksi</th>
					                </tr>
					              </thead>
					              <tbody>
					                <tr>
					                	<?php
											$sqlproduct = "SELECT p.id as id, p.name as pname, p.price as price, pt.name as ptname FROM product p INNER JOIN product_type pt ON p.type_id = pt.id";
											$resultproduct= mysql_query($sqlproduct);	
											while($product = mysql_fetch_array($resultproduct)){
												echo "<td>".$product['pname']."</td>
												<td>".$product['price']."</td>
												<td>".$product['ptname']."</td>
												<td>
													<a href='proses/image.php?action=index&prod_id=".$product['id']."' title='Lihat'><i class='glyphicon glyphicon-eye-open'></i></a>
													<a href='proses/product.php?action=edit&id=".$product['id']."' title='Edit'><i class='glyphicon glyphicon-edit'></i></a>
													<a href='#' data-toggle='modal', data-target='#productModal".$product['id']."' title='Hapus'><i class='glyphicon glyphicon-trash'></i></a>
													<div class='modal fade' id='productModal".$product['id']."' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
													  <div class='modal-dialog' style='width:450px'>
													    <div class='modal-content'>
													      <div class='modal-header' align='center'>
													        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
													        <h4 class='modal-title' id='myModalLabel'>Hapus Produk</h4>
													      </div>
													      <div class='modal-body'><h5>Apakah Anda yakin akan menghapus data ".$product['pname']." ?</h5></div>
													      <div class='modal-footer'>
													      	<a href='proses/product.php?action=del&name=".$product['pname']."' class='btn btn-danger' role='button'> <i class='glyphicon glyphicon-trash'></i> &nbsp; Hapus</a>
													      </div>
													    </div>
													  </div>
													</div>
												</td></tr>";
											}
					                	?>
					                </tr>
					              </tbody>
					            </table>
			  				</div>
			  			</div>
	  				</div>

	  				<div class="col-md-5">
	  					<div class="content-box-large">
			  				<div class="panel-heading">
			  				<?php if (!isset($_SESSION['name'])) { ?>
					        <div class="panel-title">Add New Product</div>
					      <?php } else { ?>  	
					        <div class="panel-title">Edit Product</div>
					      <?php } ?>
					        <div class="panel-options">
					        	<a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
					          <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
					        </div>
					      </div>
					    	
					    	<?php if (isset($_SESSION['name'])) { ?>
					    	<div class="panel-body">
			  					<form class="form-horizontal" role="form" id="formProduct" action="proses/product.php?action=update" method="POST">
								  <div class="form-group">
								    <label class="col-sm-2 control-label">Product Name</label>
								    <div class="col-sm-10">
								      <input type="text" required class="form-control" id="productname" name="productname" value='<?php echo $_SESSION['name']; ?>'>
								    </div>
								  </div>
								  <div class="form-group">
								    <label class="col-sm-2 control-label">Product Type</label>
								    <div class="col-sm-10">
								    	<select name="prodtype" id="prodtype" class="form-control" 
								    	value="<?php echo $_SESSION['type']; ?>">
								    		<option value='0'></option>
									  <?php 
									  	$sqltipe = 'SELECT id, name FROM product_type';
									  	$resulttipe = mysql_query($sqltipe);
									  	while ($tipe = mysql_fetch_array($resulttipe)) {
									  		if ($tipe['name'] == $_SESSION['type']) {
									  			echo '<option value='.$tipe['id'].' selected="selected">'.$tipe['name'].'</option>';
									  		}
									  		echo '<option value='.$tipe['id'].'>'.$tipe['name'].'</option>';
									  	}
									  ?></select>
								    </div>
								  </div>
								  <div class="form-group">
								    <label class="col-sm-2 control-label">Product Detail</label>
								    <div class="col-sm-10">
								      <textarea required class="form-control" id="proddetail" name="proddetail"><?php echo $_SESSION['detail']; ?></textarea>
								    </div>
								  </div>
								  <div class="form-group">
								    <label class="col-sm-2 control-label">Product Price</label>
								    <div class="col-sm-10">
								      <input type="text" required class="form-control" id="productprice" name="productprice" value="<?php echo $_SESSION['price']; ?>">
								    </div>
								  </div>
								  <div class="form-group">
								    <div class="col-sm-offset-2 col-sm-10">
								      <button type="submit" class="btn btn-primary">Update</button>
								      <input name="curname" type="hidden" value="<?php echo $_SESSION['name']; ?>">
								    </div>
								  </div>
								</form>
					    	<?php }else{ ?>

			  				<div class="panel-body">
			  					<form class="form-horizontal" role="form" id="formProduct" action="proses/product.php?action=add" method="POST">
										<?php if($_GET['notif'] == 1){ ?>
									<div class="alert alert-success">
										<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									  <strong>Product </strong> berhasil disimpan
									</div>
			  						<?php } else if($_GET['notif'] == 2){ ?>
									<div class="alert alert-danger">
										<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									  <strong>Product </strong> gagal tersimpan !!
									</div>
				            <?php } else if($_GET['notif'] == 3){ ?>
									<div class="alert alert-danger">
										<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									  <strong>Product Name </strong> telah terdaftar
									</div>
										<?php } else if($_GET['notif'] == 4){ ?>
									<div class="alert alert-success">
										<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									  <strong>Product </strong> berhasil diupdate
									</div>
										<?php } else if($_GET['notif'] == 5){ ?>
									<div class="alert alert-success">
										<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									  <strong>Product </strong> berhasil dihapus
									</div>
										<?php } ?>

								  <div class="form-group">
								    <label class="col-sm-2 control-label">Product Name</label>
								    <div class="col-sm-10">
								      <input type="input" required class="form-control" id="productname" name="productname" placeholder="Product Name">
								    </div>
								  </div>
								  <div class="form-group">
								    <label class="col-sm-2 control-label">Product Type</label>
								    <div class="col-sm-10">
								    	<select name="prodtype" id="prodtype" class="form-control">
								    		<option value='0'></option>
									  <?php 
									  	$sqltipe = 'SELECT id, name FROM product_type';
									  	$resulttipe = mysql_query($sqltipe);
									  	while ($tipe = mysql_fetch_array($resulttipe)) {
									  		echo '<option value='.$tipe['id'].'>'.$tipe['name'].'</option>';
									  	}
									  ?></select>
								    </div>
								  </div>
								  <div class="form-group">
								    <label class="col-sm-2 control-label">Product Detail</label>
								    <div class="col-sm-10">
								      <textarea required class="form-control" id="proddetail" name="proddetail"></textarea>
								    </div>
								  </div>
								  <div class="form-group">
								    <label class="col-sm-2 control-label">Product Price</label>
								    <div class="col-sm-10">
								      <input type="text" required class="form-control" id="productprice" name="productprice" placeholder="Product Price">
								    </div>
								  </div>
								  <div class="form-group">
								    <div class="col-sm-offset-2 col-sm-10">
								      <button type="submit" class="btn btn-primary">Simpan</button>
								    </div>
								  </div>
								</form> <?php }?>
			  				</div>
			  			</div>
	  				</div>
		  		</div>
		  </div>
	  		<!--  Page content -->
<?php
	unset($_SESSION['name']);
	unset($_SESSION['type']);
	unset($_SESSION['detail']);
	unset($_SESSION['price']);
?>